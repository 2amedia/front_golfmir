'use strict';

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),//ошибки
    watch = require('gulp-watch'),//следит за файлами
    prefixer = require('gulp-autoprefixer'),//добавление префиксов
    sass = require('gulp-sass'),//sass компиляция
    fileinclude = require('gulp-file-include'),//
    cleanCSS = require('gulp-clean-css'),//минификация
    imagemin = require('gulp-imagemin'),//
    pngquant = require('imagemin-pngquant'),//
    concat = require('gulp-concat'),//объединение файлов
    index = require('gulp-index'),
    browserSync = require("browser-sync"),//
    reload = browserSync.reload,
    clean = require('gulp-clean'),
    rimraf = require('gulp-rimraf'),
    fs = require('fs'),
    fontfacegen = require('fontfacegen'),
    path = require('path');
	
//определим основные пути
var dir = {
    //Тут мы укажем куда складывать готовые после сборки файлы
	css:{
		src:'src/**/*.css',
		watch:'src/**/*.css',
		build:'build/'
	},
	scss:{
		src:'src/**/*.scss',
		watch:'src/**/*.scss',
		build:'build/'
	},
	
	html:{
		src:'src/html/[^_]*.html',
		watch:'src/html/*.html',
		build:'build/'
	},
	
	js:{
		src: 'src/**/*.js',
		watch:'src/**/*.js',
		build:'build/'
	},
	
	img:{
		src: 'src/img/**/*.*',
		build:'build/img/',
		watch:'src/img/**/*.*'
	},
	
	fonts:{
		src:'src/fonts_src/',
		watch:'src/fonts_src/',
		build:'build/fonts/'
	},
	
	clean: './build'
 
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    //host: '192.168.1.37',
    port: 5090,
    logPrefix: "2AMEDIA"
};


gulp.task('html:build', function () {
    gulp.src(dir.html.src)
        .pipe(plumber())
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(dir.html.build))
        .pipe(plumber.stop())
		.pipe(index({'relativePath': 'build',}))
		.pipe(gulp.dest(dir.html.build))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(dir.js.src)
        .pipe(gulp.dest(dir.js.build))
        .pipe(reload({stream: true}));
});

//Напишем задачу для сборки нашего SCSS:
gulp.task('scss:build', function () {
    gulp.src(dir.scss.src) //Выберем наш main.scss
        .pipe(plumber())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(plumber.stop())
        .pipe(gulp.dest(dir.scss.build))
        .pipe(reload({stream: true}));
});


gulp.task('css:build', function () {
    gulp.src(dir.css.src)
        .pipe(gulp.dest(dir.css.build))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
    gulp.src(dir.img.src)
        .pipe(plumber())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(dir.img.build)) //И бросим в build
        .pipe(plumber.stop())
        .pipe(reload({stream: true}));
});

//компиляция шрифтов
gulp.task('fonts:build', function () {
    var fonts = fs.readdirSync(dir.fonts.src);
    for (var i = fonts.length - 1; i >= 0; i--) {
        var font = fonts[i];
        var extension = path.extname(font);
        if (extension == '.ttf' || extension == '.otf') {
            fontfacegen({
                source: path.join(dir.fonts.src, font),
                dest: dir.fonts.build,
                css: dir.fonts.build+'fonts.css',
                collate: true
            });
        }
    }
});
gulp.task('build', [
    'html:build',
    'js:build',
    'scss:build',
    'css:build',
    'image:build'
]);
//Чтобы не лазить все время в консоль давайте попросим gulp каждый раз при изменении какого то файла запускать нужную задачу
gulp.task('watch', function () {
    watch([dir.html.watch], function (event, cb) {
        gulp.start('html:build');
    });
    watch(dir.css.watch, function (event, cb) {
        gulp.start('css:build');
    });
    watch(dir.scss.watch, function (event, cb) {
        gulp.start('scss:build');
    });
    watch(dir.js.watch, function (event, cb) {
        gulp.start('js:build');
    });
     watch(dir.img.watch, function (event, cb) {
        gulp.start('img:build');
    });
});

//livereload — нам необходимо создать себе локальный веб-сервер. Для этого напишем следующий простой таск
gulp.task('webserver', function () {
    browserSync(config);
});

// Теперь при запуске команды gulp clean просто будет удаляться папка build.
gulp.task('clean', function () {
    return gulp.src('./build/', {read: false})
        .pipe(clean());
});
//мы определим дефолтный таск, который будет запускать всю нашу сборку.
gulp.task('default', ['build', 'webserver', 'watch']);
