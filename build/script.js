$(document).ready(function () {

//top menu

    // no-mobile

    $(".dropdown").mouseover(function () {
        if (!$('#navbar1').hasClass('show')) {
            $(this).addClass('show').attr('aria-expanded', "true");
            $(this).find('.dropdown-menu').addClass('show');
        }
    });

    $(".dropdown").mouseout(function () {
        if (!$('#navbar1').hasClass('show')
        ) {
            $(this).removeClass('show').attr('aria-expanded', "false");
            $(this).find('.dropdown-menu').removeClass('show');
        }
    });


    // top menu mobile

    $(".dropdown").on('click', function () {
        // if (
        //     $('#navbar1').hasClass('show')
        // ) {
            $(".dropdown").removeClass('show');
            $('.dropdown-menu').removeClass('show');
            $(this).addClass('show').attr('aria-expanded', "true");
            $(this).find('.dropdown-menu').addClass('show');
        // }
    });

    $('.dropdown-link').on('click', function (e) {
        if (!$(this).parent('.dropdown').hasClass('show')) {
            e.preventDefault();
        }
    });


    /* search-form */

    $(".btn-mob-search").click(function(){
        // $(".logo").toggleClass("none");
        $(".form-search").toggleClass("active");
        $("input[type='text']").focus();
    });

    $(".img-arr").click(function(){
        $(this).siblings(".dropdown-menu").toggleClass("show");
    });

    /* sliders */

    var windowsize = $(window).width();

    $(".news-slider").slick({
        vertical: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
    });

    $(".calendar-slider-index").slick({
        vertical: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        infinite: true,
    });

    $(".golf-clubs-slider").slick({
        vertical: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        infinite: true,
    });

    if ($('.news-horizontal-slider').length) {
        if (windowsize <= 992) {
            $(".news-horizontal-slider").slick({
                slidesToShow: 1,
                infinite : false
            });
        }
        else{
            $(".news-horizontal-slider").slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite : false
            });
        }
    }
    if ($('.calendar-slider').length) {
        if (windowsize <= 992) {
            $(".calendar-slider").slick({
                vertical: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
            });
            $(".calendar-slider .slick-prev").click(function () {
                $(this).css({
                    "background-image" : "url('/img/arr-green-up.svg')",
                    "background-repeat": "no-repeat",
                    "background-position": "center",
                    "background-size": "contain"});
                $(".calendar-slider .slick-next").css({
                    "background-image" : "url('/img/arr-gray-down.svg')",
                    "background-repeat": "no-repeat",
                    "background-position": "center",
                    "background-size": "contain"});
            });


            $(".calendar-slider .slick-next").click(function () {
                $(this).css({
                    "background-image" : "url('/img/arr-green-down.svg')",
                    "background-repeat": "no-repeat",
                    "background-position": "center",
                    "background-size": "contain"});

                $(".calendar-slider .slick-prev").css({
                    "background-image" : "url('/img/arr-gray-up.svg')",
                    "background-repeat": "no-repeat",
                    "background-position": "center",
                    "background-size": "contain"});
            });
        } else{
            $(".calendar-slider").slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                infinite: true,

            });
        }
    }



    let logoSlidesCount = $('.seminars-row').find('.seminar').length;  //slider logos companies

    if (logoSlidesCount > 4) {
        if (windowsize <= 992) {
            $(".seminars-row").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true

            });
        } else {
            $(".seminars-row").slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true
            });
        }
    }

    if ($('.calendar-slider-horizontal').length) {
        if (windowsize <= 992) {
            $(".calendar-slider-horizontal").slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite : false
            });
        }
        else{
            $(".calendar-slider-horizontal").slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                infinite : true
            });
        }
    }

    $(".club-news-slider").slick({
        vertical: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        infinite: true,
    });

    if ($('.other-clubs-slider').length) {
        if (windowsize <= 992) {
            $(".other-clubs-slider").slick({
                slidesToShow: 1,
                infinite: false,
            });
        } else {
            $(".other-clubs-slider").slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: false,
            });
        }
    }




    $(".calendar-slider-news").slick({
        vertical: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
    });

    $(".teachers-slider").slick();


    $('.golf-club-slider-for').slick({
        slidesToShow: 1,
        infinite: false,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.golf-club-slider-nav'
    });
    $('.golf-club-slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.golf-club-slider-for',
        focusOnSelect: true
    });

    $('.article-slider-for').slick({
        slidesToShow: 1,
        infinite: false,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.article-slider-nav'
    });
    $('.article-slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.article-slider-for',
        focusOnSelect: true
    });

    $('.detail-product-slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.detail-product-slider-nav',

    });
    $('.detail-product-slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.detail-product-slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true,

    });

    $('.gallery-slider-for').slick({
        slidesToShow: 1,
        infinite: true,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        centerMode: true,
        asNavFor: '.gallery-slider-nav',
        // variableWidth: true,
        responsive: true,

    });
    $('.gallery-slider-nav').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.gallery-slider-for',
        infinite: true,
        // dots: true,
        // centerMode: true,
        focusOnSelect: true,
    });


    $('.detail-turnir-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
    });


    $('.row-date:last', '.product-text:last')
        .css('border-bottom', 'none');



    /* selestrics */

    $("#clubs").selectric();
    $("#sort").selectric();
    $("#continent").selectric();
    $("#country").selectric();
    $("#sort-date").selectric();
    $("#sort-price").selectric();
    $("#sort-place").selectric();
    $("#sort-search").selectric();


    $('.filters-mob').click(function () {
        $(this).hide(200);
        $('.selectric-mob').fadeIn(400);
    });


    $(".news-container .slick-prev").click(function () {
        $(this).css({"z-index" : "1",
            "background-image" : "url('/img/next-green.png')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
        $(".news-container .slick-next").css({
            "z-index" : "0",
            "background-image" : "url('/img/prev-gray.png')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });

    $(".news-container .slick-next").click(function () {
        $(this).css({"z-index" : "1",
            "background-image" : "url('/img/next-green.png')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});

        $(".news-container .slick-prev").css({
            "z-index" : "0",
            "background-image" : "url('/img/prev-gray.png')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });

    $(".calendar-slider-index .slick-prev").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});

        $(".calendar-slider-index .slick-next").css({
            "background-image" : "url('/img/arr-gray-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });

    $(".calendar-slider-index .slick-next").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});

        $(".calendar-slider-index .slick-prev").css({
            "background-image" : "url('/img/arr-gray-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });

    $(".golf-clubs-slider .slick-prev").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "center"});
        $(".golf-clubs-slider .slick-next").css({
            "background-image" : "url('/img/arr-gray-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});

    });

    $(".news-slider .slick-prev").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
        $(".news-slider .slick-next").css({
            "background-image" : "url('/img/arr-gray-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });

    $(".news-slider .slick-next").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});


        $(".news-slider .slick-prev").css({
            "z-index" : "0",
            "background-image" : "url('/img/arr-gray-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });


    $(".club-news-slider .slick-prev").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
        $(".club-news-slider .slick-next").css({
            "background-image" : "url('/img/arr-gray-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});

    });

    $(".club-news-slider .slick-next").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});

        $(".club-news-slider .slick-prev").css({
            "background-image" : "url('/img/arr-gray-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });

        $(".golf-clubs-slider .slick-next").click(function () {
            $(this).css({
                "background-image" : "url('/img/arr-green-down.svg')",
                "background-repeat": "no-repeat",
                "background-position": "center",
                "background-size": "contain"});

            $(".golf-clubs-slider .slick-prev").css({
                "background-image" : "url('/img/arr-gray-up.svg')",
                "background-repeat": "no-repeat",
                "background-position": "center",
                "background-size": "contain"});
        });




        $(".style .bx-next").click(function () {
            $(this).css({"background" : "#1E554F", "z-index" : "1",
                "background-image" : "url('/img/arr-white-next.png')",
                "background-repeat": "no-repeat",
                "background-position": "center 11px",
                "background-size": "inherit"});

            $(".style .bx-prev").css({"background" : "#F5F7F7",
                "z-index" : "0",
                "background-image" : "url('/img/arr-green-prev.png')",
                "background-repeat": "no-repeat",
                "background-position": "21px 11px",
                "background-size": "inherit"});
        });


    $(".style .bx-prev").click(function () {
        $(this).css({"background" : "#1E554F", "z-index" : "1",
            "background-image" : "url('/img/arr-white-prev.png')",
            "background-repeat": "no-repeat",
            "background-position": "center 11px",
            "background-size": "inherit"});
        $(".style .bx-next").css({"background" : "#F5F7F7",
            "z-index" : "0",
            "background-image" : "url('/img/arr-green-next.png')",
            "background-repeat": "no-repeat",
            "background-position": "center 11px",
            "background-size": "inherit"});

    });

    $(".calendar-slider-news .slick-prev").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
        $(".calendar-slider-news .slick-next").css({
            "background-image" : "url('/img/arr-gray-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });


    $(".calendar-slider-news .slick-next").click(function () {
        $(this).css({
            "background-image" : "url('/img/arr-green-down.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});

        $(".calendar-slider-news .slick-prev").css({
            "background-image" : "url('/img/arr-gray-up.svg')",
            "background-repeat": "no-repeat",
            "background-position": "center",
            "background-size": "contain"});
    });

        /* checkout, add and del products */

    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });


    $('#phone, #phone-buy, #phoneReg').inputmask('+7 (999) 999-99-99');      // inputmask phone


    $(".btn-redact").click( function () {
        $(this).css({"display" : "none"});
        $(".btn-none").css({"display" : "inline-block"});
        $('input').prop('disabled',false);
    });

    $(".btn-none").click( function () {
        $(".btn-none").css({"display" : "none"});
        $(".btn-redact").css({"display" : "inline-block"});
        $('input').prop('disabled',true);
    });

    $('.date-filter').datepicker({
        range: true,
        startDate: new Date()
    });


        $('.lk_link').click(function (e) {
            e.preventDefault();
            var cur = $(this).attr('href');
            $('.lk_link').removeClass('active');
            $(this).addClass('active');
            $('.en-link').addClass('d-none');
            $('#'+cur).removeClass('d-none');
        });


    $(".check-input").click(function () {
        var parent = $(this).parent(".form-check");
        $(".form-check").removeClass("bg");
        parent.addClass("bg");
    });


    // $("#forgetPassword").on("click", function () {
    //     $("#Enter").modal("hide");
    //     $("#passwordRecovery").modal("show");
    // });
    //
    // $("#backToAuthorization").on("click", function () {
    //     $("#passwordRecovery").modal("hide");
    //     $("#Enter").modal("show");
    // });
    //
    // $("#later").on("click", function () {
    //     $("#buyTurnir").modal("hide");
    //     $("#buyKaddy").modal("show");
    // });

$(".btn-like").click(function () {
    $(".btns-socials").show();
});

    $(".close-socials").click(function () {
        $(".btns-socials").hide();
    });

});